# This Python file uses the following encoding: utf-8
import jack
import sys
import os
from PySide2.QtCore import QObject, Signal, Slot
from PySide2.QtQml import QQmlApplicationEngine
from PySide2.QtGui import QGuiApplication, QIcon

# Starting and naming the JACK Client.
client = jack.Client('Jack Transport for Plasma')


class MainBackend(QObject):
    signalSkipBackButton = Signal(bool)
    signalStopButton = Signal(bool)
    signalPlayButton = Signal(bool)
    signalPauseButton = Signal(bool)
    signalSkipForwardButton = Signal(bool)

    def __init__(self):
        QObject.__init__(self)

    # Skip Back Button.
    # The frames from client.transport_query are formatted
    # and turned into an integer to be subtracted from.
    @Slot()
    def SkipBackButton(self):
        self.signalSkipBackButton.emit(True)
        position, pos = client.transport_query()
        frames = []
        frames.append(format(
            pos['frame']))
        formattedframes = "".join(frames)
        numberedframes = int(formattedframes)
        if numberedframes > 48000:
            client.transport_frame = (numberedframes - 48000)
        else:
            client.transport_frame = 0

    @Slot()
    def StopButton(self):
        self.signalStopButton.emit(True)
        client.transport_stop()
        client.transport_frame = 0

    @Slot()
    def PlayButton(self):
        self.signalPlayButton.emit(True)
        client.transport_start()

    @Slot()
    def PauseButton(self):
        self.signalPauseButton.emit(True)
        client.transport_stop()

    @Slot()
    def SkipForwardButton(self):
        self.signalSkipForwardButton.emit(True)
        position, pos = client.transport_query()
        frames = []
        frames.append(format(
            pos['frame']))
        formattedframes = "".join(frames)
        numberedframes = int(formattedframes)
        client.transport_frame = (numberedframes + 48000)


if __name__ == '__main__':
    os.environ["QT_QUICK_BACKEND"] = "software"
    application = QGuiApplication(sys.argv)
    application.setWindowIcon(QIcon("jtfp"))
    engine = QQmlApplicationEngine()
    main = MainBackend()
    engine.rootContext().setContextProperty("backend", main)
    engine.load(os.path.join(os.path.dirname(__file__), "main.qml"))
    if not engine.rootObjects():
        sys.exit(-1)
    try:
        sys.exit(application.exec_())
    except SystemExit:
        pass
