 #!/bin/sh
 
if [ -d /usr/local/share/jack-transport-for-plasma ]
then
    echo ".local/share/jtfp found"
else
    mkdir /usr/local/share/jack-transport-for-plasma
fi

if [ -d /usr/local/bin ]
then
    echo "/usr/local/bin found"
else
    mkdir /usr/local/bin
fi

if [ -d /usr/local/share/icons/scaleable ]
then
    echo "/usr/local/share/icons/scaleable found"
else
    mkdir /usr/local/share/icons/scaleable
fi

if [ -d /usr/local/share/applications ]
then
    echo "/usr/local/share/applications found"
else
    mkdir /usr/local/share/applications
fi
cp main.py /usr/local/share/jack-transport-for-plasma
cp main.qml /usr/local/share/jack-transport-for-plasma
cp jtfp.svg /usr/local/share/icons/scaleable
cp jack-transport-for-plasma /usr/local/bin
chmod a+x /usr/local/bin/jack-transport-for-plasma
cp jack-transport-for-plasma.desktop /usr/local/share/applications
echo 'Finished'
